import { FormEvent, useContext, useState } from 'react';
import { Helmet } from 'react-helmet';
import { userInfoKeyLabel } from '../common/user-info-key-label.enum';
import { IRegForm } from '../interfaces/IRegForm';
import {
  containsLettersOnly,
  containsNumber,
  containsOnlyLettersAndDigits,
  isValidEmail,
  isValidPhoneNumber,
} from '../helpers/helpers';
import { ToastContext } from '../providers/toast-context';
import { toastClass } from '../common/toast-class.enum';
import { createUser, getAllUsers, getUser } from '../services/users.service';
import { loginUser, registerUser } from '../services/auth.service';
import { AuthContext } from '../providers/auth-context';
import { useNavigate } from 'react-router-dom';
import { userInfoValidation } from '../common/user-info-validation.enum';
import { userInfoPlaceholder } from '../common/user-info-placeholder.enum';
import { userInfoDefaultValue } from '../common/user-info-default-value.enum';

export default function Register() {
  const { addToast } = useContext(ToastContext);
  const { setAppState } = useContext(AuthContext);
  const navigate = useNavigate();
  const [isFormValid, setIsFormValid] = useState(false);
  const [form, setForm] = useState({
    email: {
      value: userInfoDefaultValue.EMAIL,
      placeholder: userInfoPlaceholder.EMAIL,
      validation: {
        minLength: userInfoValidation.EMAIL_MIN_LENGTH,
        maxLength: userInfoValidation.EMAIL_MAX_LENGTH,
        isValidEmail: userInfoValidation.IS_VALID_EMAIL,
      },
      touched: userInfoDefaultValue.EMAIL_TOUCHED,
      valid: userInfoDefaultValue.EMAIL_VALID,
      error: userInfoDefaultValue.EMAIL_ERROR,
    },
    firstName: {
      value: userInfoDefaultValue.FIRST_NAME,
      placeholder: userInfoPlaceholder.FIRST_NAME,
      validation: {
        minLength: userInfoValidation.FIRST_NAME_MIN_LENGTH,
        maxLength: userInfoValidation.FIRST_NAME_MAX_LENGTH,
        lettersOnly: userInfoValidation.LETTERS_ONLY,
      },
      touched: userInfoDefaultValue.FIRST_NAME_TOUCHED,
      valid: userInfoDefaultValue.EMAIL_VALID,
      error: userInfoDefaultValue.EMAIL_ERROR,
    },
    lastName: {
      placeholder: userInfoPlaceholder.LAST_NAME,
      value: userInfoDefaultValue.LAST_NAME,
      validation: {
        minLength: userInfoValidation.LAST_NAME_MIN_LENGTH,
        maxLength: userInfoValidation.LAST_NAME_MAX_LENGTH,
        lettersOnly: userInfoValidation.LETTERS_ONLY,
      },
      touched: userInfoDefaultValue.LAST_NAME_TOUCHED,
      valid: userInfoDefaultValue.LAST_NAME_VALID,
      error: userInfoDefaultValue.LAST_NAME_ERROR,
    },
    phoneNumber: {
      placeholder: userInfoPlaceholder.PHONE_NUMBER,
      value: userInfoDefaultValue.PHONE_NUMBER,
      validation: {
        isValidPhoneNumber: userInfoValidation.IS_VALID_PHONE_NUMBER,
      },
      touched: userInfoDefaultValue.PHONE_NUMBER_TOUCHED,
      valid: userInfoDefaultValue.PHONE_NUMBER_VALID,
      error: userInfoDefaultValue.PHONE_NUMBER_ERROR,
    },
    username: {
      value: userInfoDefaultValue.USERNAME,
      placeholder: userInfoPlaceholder.USERNAME,
      validation: {
        minLength: userInfoValidation.USERNAME_MIN_LENGTH,
        maxLength: userInfoValidation.USERNAME_MAX_LENGTH,
        lettersAndDigitsOnly: userInfoValidation.LETTERS_AND_DIGITS_ONLY,
        startsWithLetter: userInfoValidation.STARTS_WITH_LETTER,
      },
      touched: userInfoDefaultValue.USERNAME_TOUCHED,
      valid: userInfoDefaultValue.USERNAME_VALID,
      error: userInfoDefaultValue.USERNAME_ERROR,
    },
    password: {
      value: userInfoDefaultValue.PASSWORD,
      placeholder: userInfoPlaceholder.PASSWORD,
      validation: {
        minLength: userInfoValidation.PASSWORD_MIN_LENGTH,
        maxLength: userInfoValidation.PASSWORD_MAX_LENGTH,
        hasNumber: userInfoValidation.HAS_NUMBER,
      },
      touched: userInfoDefaultValue.PASSWORD_TOUCHED,
      valid: userInfoDefaultValue.PASSWORD_VALID,
      error: userInfoDefaultValue.PASSWORD_ERROR,
    },
  });
  const { email, firstName, lastName, phoneNumber, username, password } = form;

  const inputChangeHandler = (event: FormEvent<HTMLInputElement>) => {
    const { name, value } = event.currentTarget;
    const updateElement = form[name as keyof IRegForm];

    updateElement.touched = true;
    updateElement.valid = isInputValid(value, name);
    updateElement.value = value;

    if (!updateElement.valid) {
      if (name === userInfoKeyLabel.EMAIL && !isValidEmail(value)) {
        updateElement.error = `Invalid email pattern`;
      } else if (
        [userInfoKeyLabel.FIRST_NAME, userInfoKeyLabel.LAST_NAME].includes(
          name
        ) &&
        value.length > 0 &&
        !containsLettersOnly(value)
      ) {
        updateElement.error = `Only English letters allowed`;
      } else if (
        name === userInfoKeyLabel.PHONE_NUMBER &&
        !isValidPhoneNumber(value)
      ) {
        updateElement.error = `Invalid Phone Number (exactly 10 digits required)`;
      } else if (name === userInfoKeyLabel.PASSWORD && !containsNumber(value)) {
        updateElement.error = `At least 1 digit required`;
      } else if (
        name === userInfoKeyLabel.USERNAME &&
        !containsLettersOnly(value[0])
      ) {
        updateElement.error = `Username should start with letter`;
      } else if (
        name === userInfoKeyLabel.USERNAME &&
        !containsOnlyLettersAndDigits(value)
      ) {
        updateElement.error = `Only numbers and English letters allowed`;
      } else if (
        'minLength' in updateElement.validation &&
        'maxLength' in updateElement.validation
      ) {
        updateElement.error =
          value.length < updateElement.validation.minLength
            ? `Minimum ${updateElement.placeholder} lenght: ${updateElement.validation.minLength}`
            : `Maximum ${updateElement.placeholder} lenght: ${updateElement.validation.maxLength}`;
      }
    }

    setForm((prevForm) => ({ ...prevForm, [name]: updateElement }));

    const formValid = Object.values(form).every((element) => element.valid);
    setIsFormValid(formValid);
  };

  const isInputValid = (value: string, name: string) => {
    const updateElement = form[name as keyof IRegForm];
    let isValid = true;

    if ('minLength' in updateElement.validation) {
      isValid = isValid && value.length >= updateElement.validation.minLength;
    }
    if ('maxLength' in updateElement.validation) {
      isValid = isValid && value.length <= updateElement.validation.maxLength;
    }
    if ('lettersOnly' in updateElement.validation) {
      isValid = isValid && containsLettersOnly(value);
    }
    if ('hasNumber' in updateElement.validation) {
      isValid = isValid && containsNumber(value);
    }
    if ('isValidPhoneNumber' in updateElement.validation) {
      isValid = isValid && isValidPhoneNumber(value);
    }
    if ('startsWithLetter' in updateElement.validation) {
      isValid = isValid && containsLettersOnly(value[0]);
    }
    if ('lettersAndDigitsOnly' in updateElement.validation) {
      isValid = isValid && containsOnlyLettersAndDigits(value);
    }
    if ('isValidEmail' in updateElement.validation) {
      isValid = isValid && isValidEmail(value);
    }

    return isValid;
  };

  const registerHandler = async (event: FormEvent) => {
    event.preventDefault();

    if (!isFormValid) return addToast(toastClass.ERROR, 'Invalid form');

    try {
      const user = await getUser(username.value);

      if (user !== null)
        return addToast(
          toastClass.ERROR,
          `User with username ${username.value} already exists!`
        );

      const users = await getAllUsers();

      if (users.find((u) => u.phonenumber === phoneNumber.value)) {
        return addToast(
          toastClass.ERROR,
          `User with phone number ${phoneNumber.value} already exists!`
        );
      }

      const credentials = await registerUser(email.value, password.value);

      try {
        const userData = await createUser(
          credentials.user.uid,
          credentials.user.email,
          firstName.value,
          lastName.value,
          username.value,
          phoneNumber.value
        );

        setAppState((appState) => ({
          ...appState,
          userData,
        }));

        addToast(toastClass.SUCCESS, 'You have been registered!');
      } catch (e) {
        return addToast(toastClass.ERROR, (e as Error).message);
      }

      try {
        const credentials = await loginUser(email.value, password.value);

        setAppState((appState) => ({
          ...appState,
          user: {
            email: credentials.user.email,
            uid: credentials.user.uid,
          },
        }));

        addToast(toastClass.INFO, 'You have been logged in!');
        navigate('/');
      } catch (e) {
        addToast(toastClass.ERROR, 'Something went wrong!');
      }
    } catch (e) {
      if ((e as Error).message.includes('auth/email-already-in-use')) {
        return addToast(
          toastClass.ERROR,
          'This email has already been registered!'
        );
      }

      addToast(toastClass.ERROR, 'Something went wrong!');
    }
  };

  return (
    <>
      <Helmet>
        <title>Login</title>
      </Helmet>
      <h1 className="mt-24">Register form:</h1>
      <form
        onSubmit={registerHandler}
        className="flex flex-col w-full max-w-[18rem]"
      >
        <div className="flex flex-col mt-4 mb-2">
          <input
            type="text"
            name={userInfoKeyLabel.EMAIL}
            placeholder={email.placeholder}
            className={`input input-bordered input-primary h-10 w-full focus:outline-0 ${
              email.touched && email.valid
                ? 'focus:border-accent'
                : 'focus:border-error'
            }`}
            required
            onChange={inputChangeHandler}
          />
          {email.touched && !email.valid && (
            <span className="text-error text-sm">{email.error}</span>
          )}
        </div>
        <div className="flex flex-col my-2">
          <input
            type="text"
            name={userInfoKeyLabel.FIRST_NAME}
            placeholder={firstName.placeholder}
            className={`input input-bordered input-primary h-10 w-full focus:outline-0 ${
              firstName.touched && firstName.valid
                ? 'focus:border-accent'
                : 'focus:border-error'
            }`}
            required
            onChange={inputChangeHandler}
          />
          {firstName.touched && !firstName.valid && (
            <span className="text-error text-sm">{firstName.error}</span>
          )}
        </div>
        <div className="flex flex-col my-2">
          <input
            type="text"
            name={userInfoKeyLabel.LAST_NAME}
            placeholder={lastName.placeholder}
            className={`input input-bordered input-primary h-10 w-full focus:outline-0 ${
              lastName.touched && lastName.valid
                ? 'focus:border-accent'
                : 'focus:border-error'
            }`}
            required
            onChange={inputChangeHandler}
          />
          {lastName.touched && !lastName.valid && (
            <span className="text-error text-sm">{lastName.error}</span>
          )}
        </div>
        <div className="flex flex-col my-2">
          <input
            type="text"
            name={userInfoKeyLabel.PHONE_NUMBER}
            placeholder={phoneNumber.placeholder}
            className={`input input-bordered input-primary h-10 w-full focus:outline-0 ${
              phoneNumber.touched && phoneNumber.valid
                ? 'focus:border-accent'
                : 'focus:border-error'
            }`}
            required
            onChange={inputChangeHandler}
          />
          {phoneNumber.touched && !phoneNumber.valid && (
            <span className="text-error text-sm">{phoneNumber.error}</span>
          )}
        </div>
        <div className="flex flex-col my-2">
          <input
            type="text"
            name={userInfoKeyLabel.USERNAME}
            placeholder={username.placeholder}
            className={`input input-bordered input-primary h-10 w-full focus:outline-0 ${
              username.touched && username.valid
                ? 'focus:border-accent'
                : 'focus:border-error'
            }`}
            required
            onChange={inputChangeHandler}
          />
          {username.touched && !username.valid && (
            <span className="text-error text-sm">{username.error}</span>
          )}
        </div>
        <div className="flex flex-col my-2">
          <input
            type="password"
            name={userInfoKeyLabel.PASSWORD}
            placeholder={password.placeholder}
            className={`input input-bordered input-primary h-10 w-full focus:outline-0 ${
              password.touched && password.valid
                ? 'focus:border-accent'
                : 'focus:border-error'
            }`}
            required
            onChange={inputChangeHandler}
          />
          {password.touched && !password.valid && (
            <span className="text-error text-sm">{password.error}</span>
          )}
        </div>
        <button type="submit" className="btn btn-outline border-primary my-4">
          Register
        </button>
      </form>
    </>
  );
}
