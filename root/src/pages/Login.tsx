import { FormEvent, useContext, useState } from 'react';
import { Helmet } from 'react-helmet';
import { Link, useNavigate } from 'react-router-dom';
import { getAllUsers } from '../services/users.service';
import { ToastContext } from '../providers/toast-context';
import { toastClass } from '../common/toast-class.enum';
import { loginUser } from '../services/auth.service';
import { AuthContext } from '../providers/auth-context';

export default function Login() {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const { addToast } = useContext(ToastContext);
  const { setAppState } = useContext(AuthContext);
  const navigate = useNavigate();

  const loginHandler = async (event: FormEvent) => {
    event.preventDefault();

    try {
      const user = (await getAllUsers()).find((u) => u.email === email);

      if (user !== undefined && user.blocked) {
        return addToast(toastClass.ERROR, 'Your account is blocked!');
      }

      const credentials = await loginUser(email, password);

      setAppState((appState) => ({
        ...appState,
        user: {
          email: credentials.user.email,
          uid: credentials.user.uid,
        },
      }));

      addToast(toastClass.INFO, 'You have been logged in!');

      navigate('/');
    } catch (e) {
      return addToast(toastClass.ERROR, (e as Error).message);
    }
  };

  return (
    <>
      <Helmet>
        <title>Login</title>
      </Helmet>
      <h1 className="mt-24">Please enter your credentials</h1>
      <form
        onSubmit={loginHandler}
        className="flex flex-col w-full max-w-[18rem]"
      >
        <input
          type="text"
          name="email"
          placeholder="Email"
          value={email}
          onChange={(e) => setEmail(e.target.value)}
          className="input input-bordered input-primary h-10 w-full my-4 focus:outline-0 focus:border-accent"
          required
        />
        <input
          type="password"
          name="password"
          placeholder="Password"
          value={password}
          onChange={(e) => setPassword(e.target.value)}
          className="input input-bordered input-primary h-10 w-full focus:outline-0 focus:border-accent"
          required
        />
        <button type="submit" className="btn btn-outline border-primary my-4">
          Log In
        </button>
      </form>
      <span className="text-base">Don&apos;t have an account yet?</span>
      <Link to={'/register'} className="link link-hover text-lg">
        Sing Up now
      </Link>
    </>
  );
}
