import { ArrowUpTrayIcon } from '@heroicons/react/24/outline';
import { Helmet } from 'react-helmet';
import defaultCoverImage from '../assets/defaultCoverImage.png';
import { FormEvent, useContext, useRef, useState } from 'react';
import { IContestForm } from '../interfaces/IContestForm';
import { ToastContext } from '../providers/toast-context';
import { toastClass } from '../common/toast-class.enum';
import { contestPlaceholder } from '../common/contest-placeholder.enum';
import { contestValidation } from '../common/contest-validation.enum';
import { contestDefaultValue } from '../common/contest-default-value.enum';
import { ref, uploadBytes, getDownloadURL } from 'firebase/storage';
import { storage } from '../firebase/config';
import { createContest, isTitleUnique } from '../services/contests.service';
import { AuthContext } from '../providers/auth-context';
import { contestType } from '../common/contest-type.enum';
import { contestJuryType } from '../common/contest-jury-type.enum';

export default function NewContest() {
  const { addToast } = useContext(ToastContext);
  const { userData } = useContext(AuthContext);
  const [isFormValid, setIsFormValid] = useState(false);
  const [form, setForm] = useState({
    title: {
      value: contestDefaultValue.TITLE,
      placeholder: contestPlaceholder.TITLE,
      validation: {
        minLength: contestValidation.TITLE_MIN_LENGTH,
        maxLength: contestValidation.TITLE_MAX_LENGTH,
      },
      touched: contestDefaultValue.TITLE_TOUCHED,
      valid: contestDefaultValue.TITLE_VALID,
      error: contestDefaultValue.TITLE_ERROR,
    },
    category: {
      value: contestDefaultValue.CATEGORY,
      placeholder: contestPlaceholder.CATEGORY,
      validation: {
        minLength: contestValidation.CATEGORY_MIN_LENGTH,
        maxLength: contestValidation.CATEGORY_MAX_LENGTH,
      },
      touched: contestDefaultValue.CATEGORY_TOUCHED,
      valid: contestDefaultValue.CATEGORY_VALID,
      error: contestDefaultValue.CATEGORY_ERROR,
    },
    p1Duration: {
      placeholder: contestPlaceholder.P1_DURATION,
      value: contestDefaultValue.P1_DURATION,
      validation: {
        minValue: contestValidation.P1_DURATION_MIN_VALUE,
        maxValue: contestValidation.P1_DURATION_MAX_VALUE,
      },
      touched: contestDefaultValue.P1_DURATION_TOUCHED,
      valid: contestDefaultValue.P1_DURATION_VALID,
      error: contestDefaultValue.P1_DURATION_ERROR,
    },
    p2Duration: {
      placeholder: contestPlaceholder.P2_DURATION,
      value: contestDefaultValue.P2_DURATION,
      validation: {
        minValue: contestValidation.P2_DURATION_MIN_VALUE,
        maxValue: contestValidation.P2_DURATION_MAX_VALUE,
      },
      touched: contestDefaultValue.P2_DURATION_TOUCHED,
      valid: contestDefaultValue.P2_DURATION_VALID,
      error: contestDefaultValue.P2_DURATION_ERROR,
    },
  });
  const { title, category, p1Duration, p2Duration } = form;
  const contestTypeRef = useRef<HTMLInputElement>(null);
  const [participants, setParticipants] = useState([]);
  const contestJuryTypeRef = useRef<HTMLInputElement>(null);
  const [jurors, setJurors] = useState([]);
  const [coverImageURL, setCoverImageURL] = useState('');
  const coverImageInputRef = useRef<HTMLInputElement>(null);

  const inputChangeHandler = (event: FormEvent<HTMLInputElement>) => {
    const { name, value } = event.currentTarget;
    const updateElement = form[name as keyof IContestForm];

    updateElement.touched = true;
    updateElement.valid = isInputValid(value, name);
    updateElement.value = value;

    if (!updateElement.valid) {
      if (
        'minLength' in updateElement.validation &&
        'maxLength' in updateElement.validation
      ) {
        updateElement.error =
          value.length < updateElement.validation.minLength
            ? `Minimum ${updateElement.placeholder} lenght: ${updateElement.validation.minLength}`
            : `Maximum ${updateElement.placeholder} lenght: ${updateElement.validation.maxLength}`;
      } else if (
        'minValue' in updateElement.validation &&
        'maxValue' in updateElement.validation
      ) {
        updateElement.error =
          +value < updateElement.validation.minValue
            ? `Minimum ${updateElement.placeholder} value: ${updateElement.validation.minValue}`
            : `Maximum ${updateElement.placeholder} value: ${updateElement.validation.maxValue}`;
      }
    }

    setForm((prevForm) => ({ ...prevForm, [name]: updateElement }));

    const formValid = Object.values(form).every((element) => element.valid);
    setIsFormValid(formValid);
  };

  const isInputValid = (value: string, name: string) => {
    const updateElement = form[name as keyof IContestForm];
    let isValid = true;

    if ('minLength' in updateElement.validation) {
      isValid = isValid && value.length >= updateElement.validation.minLength;
    }
    if ('maxLength' in updateElement.validation) {
      isValid = isValid && value.length <= updateElement.validation.maxLength;
    }
    if ('minValue' in updateElement.validation) {
      isValid = isValid && +value >= updateElement.validation.minValue;
    }
    if ('maxValue' in updateElement.validation) {
      isValid = isValid && +value <= updateElement.validation.maxValue;
    }

    return isValid;
  };

  const fileUploadHandler = (event: FormEvent<HTMLInputElement>) => {
    if (!event.currentTarget.value) {
      setCoverImageURL('');
    } else if (event.currentTarget.files) {
      if (
        !event.currentTarget.files[0].type.startsWith('image') ||
        event.currentTarget.files[0].type.includes('gif')
      ) {
        return addToast(toastClass.ERROR, 'Only image files are allowed!');
      }

      const imageURL = URL.createObjectURL(event.currentTarget.files[0]);
      setCoverImageURL(imageURL);
    }
  };

  const createContestHandler = async (event: FormEvent) => {
    event.preventDefault();

    if (!isFormValid) return addToast(toastClass.ERROR, 'Invalid form!');

    const file =
      coverImageInputRef.current &&
      coverImageInputRef.current.files &&
      coverImageInputRef.current.files[0];

    if (!file || !coverImageURL)
      return addToast(toastClass.ERROR, 'Missing Cover Image!');

    const fileExtension = file.name.substring(file.name.lastIndexOf('.') + 1);
    const coverImageRef = ref(
      storage,
      `covers/${title.value}/cover.${fileExtension}`
    );

    try {
      await isTitleUnique(title.value);

      const result = await uploadBytes(coverImageRef, file);
      const imageUrl = await getDownloadURL(result.ref);

      await createContest(
        title.value,
        category.value,
        imageUrl,
        (userData as { username: string }).username,
        +p1Duration.value,
        +p2Duration.value,
        contestTypeRef.current?.checked
          ? contestType.INVITATIONAL
          : contestType.OPEN,
        contestJuryTypeRef.current?.checked
          ? contestJuryType.INVITATIONAL
          : contestJuryType.ORGANIZERS_ONLY,
        participants,
        jurors
      );

      addToast(toastClass.SUCCESS, 'Contest created successfully!');
    } catch (error) {
      addToast(toastClass.ERROR, (error as Error).message);
    }
  };

  return (
    <>
      <Helmet>
        <title>Create New Contest</title>
      </Helmet>
      <h1 className="text-xl">Create New Contest</h1>
      <form
        onSubmit={createContestHandler}
        className="w-[49%] flex flex-col items-center"
      >
        <div className="flex flex-col w-full mt-2">
          <input
            type="text"
            name={title.placeholder.toLowerCase()}
            placeholder={title.placeholder}
            value={title.value}
            className={`input input-bordered input-primary h-10 focus:outline-0 ${
              title.touched && title.valid
                ? 'focus:border-accent'
                : 'focus:border-error'
            }`}
            required
            onChange={inputChangeHandler}
          />
          {title.touched && !title.valid && (
            <span className="text-error text-sm">{title.error}</span>
          )}
        </div>
        <div className="flex flex-col w-full mt-2">
          <input
            type="text"
            name={category.placeholder.toLowerCase()}
            placeholder={category.placeholder}
            value={category.value}
            className={`input input-bordered input-primary h-10 focus:outline-0 ${
              category.touched && category.valid
                ? 'focus:border-accent'
                : 'focus:border-error'
            }`}
            required
            onChange={inputChangeHandler}
          />
          {category.touched && !category.valid && (
            <span className="text-error text-sm">{category.error}</span>
          )}
        </div>
        <div className="w-full flex flex-col mt-2 mb-4">
          <label htmlFor="phase1Duration">
            <span>{contestPlaceholder.P1_DURATION}: </span>
            <div className="badge badge-primary font-bold">
              {`${p1Duration.value} ${
                +p1Duration.value > 1
                  ? contestPlaceholder.P1_UNIT_PLURAL
                  : contestPlaceholder.P1_UNIT_SINGULAR
              }`}
            </div>
          </label>
          <input
            id="phase1Duration"
            name="p1Duration"
            type="range"
            value={p1Duration.value}
            min={p1Duration.validation.minValue}
            max={p1Duration.validation.maxValue}
            step="1"
            className="range range-xs range-primary"
            onChange={inputChangeHandler}
          />
          {p1Duration.touched && !p1Duration.valid && (
            <span className="text-error text-sm">{p1Duration.error}</span>
          )}
        </div>
        <div className="w-full flex flex-col mb-4">
          <label htmlFor="phase2Duration">
            <span>{contestPlaceholder.P2_DURATION}: </span>
            <div className="badge badge-primary font-bold">
              {`${p2Duration.value} ${
                +p2Duration.value > 1
                  ? contestPlaceholder.P2_UNIT_PLURAL
                  : contestPlaceholder.P2_UNIT_SINGULAR
              }`}
            </div>
          </label>
          <input
            id="phase2Duration"
            name="p2Duration"
            type="range"
            value={p2Duration.value}
            min={p2Duration.validation.minValue}
            max={p2Duration.validation.maxValue}
            step="1"
            className="range range-xs range-primary"
            onChange={inputChangeHandler}
          />
          {p2Duration.touched && !p2Duration.valid && (
            <span className="text-error text-sm">{p2Duration.error}</span>
          )}
        </div>
        <div className="w-full my-4 border border-primary rounded p-4">
          <label className="cursor-pointer label justify-start">
            <span className="label-text">
              {contestPlaceholder.PARTICIPANTS_UNCHECKED}
            </span>
            <input
              ref={contestTypeRef}
              type="checkbox"
              className="toggle toggle-primary mx-4"
            />
            <span className="label-text">
              {contestPlaceholder.PARTICIPANTS_CHECKED}
            </span>
          </label>
        </div>
        <div className="w-full border border-primary rounded p-4">
          <label className="cursor-pointer label justify-start">
            <span className="label-text">
              {contestPlaceholder.JURORS_UNCHECKED}
            </span>
            <input
              ref={contestJuryTypeRef}
              type="checkbox"
              className="toggle toggle-primary mx-4"
            />
            <span className="label-text">
              {contestPlaceholder.JURORS_CHECKED}
            </span>
          </label>
        </div>
        <div className="w-full border border-primary rounded mt-4 flex justify-between items-center">
          <label htmlFor="upload-btn" className="m-2 w-[49%]">
            <div className="btn btn-outline h-fit p-2 w-full">
              <ArrowUpTrayIcon className="w-8 h-8" />
              <span> {contestPlaceholder.COVER_IMAGE}</span>
            </div>
            <input
              ref={coverImageInputRef}
              id="upload-btn"
              type="file"
              name="file"
              accept="image/*"
              className="hidden"
              onChange={fileUploadHandler}
            />
          </label>
          <div className="border border-dashed border-primary rounded-xl m-2 p-2 flex items-center justify-center w-[49%]">
            <img
              src={coverImageURL ? coverImageURL : defaultCoverImage}
              alt="cover photo"
              className="rounded"
            />
          </div>
        </div>
        <button
          type="submit"
          className="btn btn-wide btn-outline border-primary my-4 self-center"
        >
          Submit
        </button>
      </form>
    </>
  );
}
