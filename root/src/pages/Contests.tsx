import { useEffect } from 'react';
import { Helmet } from 'react-helmet';
import { useAppDispatch, useAppSelector } from '../hooks/hooks';
import { fetchContestsData } from '../store/contests-actions';

export default function Contests() {
  const dispatch = useAppDispatch();
  const { contests } = useAppSelector((state) => state.contests);

  useEffect(() => {
    dispatch(fetchContestsData());
  }, []);

  return (
    <>
      <Helmet>
        <title>Contests</title>
      </Helmet>
      <h1 className="text-xl">Contests</h1>
      <div>Contests length: {contests.length}</div>
    </>
  );
}
