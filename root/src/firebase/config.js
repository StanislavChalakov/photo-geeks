import { initializeApp } from 'firebase/app';
import { getAuth } from 'firebase/auth';
import { getDatabase } from 'firebase/database';
import { getStorage } from 'firebase/storage';

const firebaseConfig = {
  apiKey: 'AIzaSyDLupvd7Y_TwIL-5PzV7GZj4lxl0bdKpjc',
  authDomain: 'photo-geeks.firebaseapp.com',
  projectId: 'photo-geeks',
  storageBucket: 'gs://photo-geeks.appspot.com',
  messagingSenderId: '425838332589',
  appId: '1:425838332589:web:2ebda52e18dd0e71791424',
  databaseURL:
    'https://photo-geeks-default-rtdb.europe-west1.firebasedatabase.app/',
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);

export const auth = getAuth(app);

// the Realtime Database handler
export const db = getDatabase(app);

export const storage = getStorage(app);
