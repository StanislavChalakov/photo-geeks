import React from 'react';
import ReactDOM from 'react-dom/client';
import App from './App';
import Layout from './UI/Layout';
import store from './store/store';
import { BrowserRouter } from 'react-router-dom';
import { AuthContextProvider } from './providers/auth-context';
import { ToastContextProvider } from './providers/toast-context';
import { ThemeContextProvider } from './providers/theme-context';
import { Provider } from 'react-redux';
import './index.css';

ReactDOM.createRoot(document.getElementById('root') as HTMLElement).render(
  <React.StrictMode>
    <BrowserRouter>
      <AuthContextProvider>
        <ToastContextProvider>
          <ThemeContextProvider>
            <Provider store={store}>
              <Layout>
                <App />
              </Layout>
            </Provider>
          </ThemeContextProvider>
        </ToastContextProvider>
      </AuthContextProvider>
    </BrowserRouter>
  </React.StrictMode>
);
