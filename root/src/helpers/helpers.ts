export function isValidEmail(str: string) {
  return /\S+@\S+\.\S+/.test(str);
}

export function containsLettersOnly(str: string) {
  return /^[a-zA-Z]+$/.test(str);
}

export function isValidPhoneNumber(str: string) {
  return /[0]{1}[8]{1}[0-9]{8}$/.test(str);
}

export function containsNumber(str: string) {
  return /\d/.test(str);
}

export function containsOnlyLettersAndDigits(str: string) {
  return /^[a-zA-Z0-9]+$/.test(str);
}
