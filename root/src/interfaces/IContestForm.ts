export interface IContestForm {
  title: {
    value: string;
    placeholder: string;
    validation: {
      minLength: number;
      maxLength: number;
    };
    touched: boolean;
    valid: boolean;
    error: string;
  };
  category: {
    value: string;
    placeholder: string;
    validation: {
      minLength: number;
      maxLength: number;
    };
    touched: boolean;
    valid: boolean;
    error: string;
  };
  p1Duration: {
    placeholder: string;
    value: string;
    validation: {
      minValue: number;
      maxValue: number;
    };
    touched: boolean;
    valid: boolean;
    error: string;
  };
  p2Duration: {
    placeholder: string;
    value: string;
    validation: {
      minValue: number;
      maxValue: number;
    };
    touched: boolean;
    valid: boolean;
    error: string;
  };
}
