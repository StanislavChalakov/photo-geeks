export interface IRegForm {
  email: {
    value: string;
    placeholder: string;
    validation: {
      minLength: number;
      maxLength: number;
      isValidEmail: boolean;
    };
    touched: boolean;
    valid: boolean;
    error: string;
  };
  firstName: {
    value: string;
    placeholder: string;
    validation: {
      minLength: number;
      maxLength: number;
      lettersOnly: boolean;
    };
    touched: boolean;
    valid: boolean;
    error: string;
  };
  lastName: {
    placeholder: string;
    value: string;
    validation: {
      minLength: number;
      maxLength: number;
      lettersOnly: boolean;
    };
    touched: boolean;
    valid: boolean;
    error: string;
  };
  phoneNumber: {
    placeholder: string;
    value: string;
    validation: {
      isValidPhoneNumber: boolean;
    };
    touched: boolean;
    valid: boolean;
    error: string;
  };
  username: {
    value: string;
    placeholder: string;
    validation: {
      minLength: number;
      maxLength: number;
      lettersAndDigitsOnly: boolean;
      startsWithLetter: boolean;
    };
    touched: boolean;
    valid: boolean;
    error: string;
  };
  password: {
    value: string;
    placeholder: string;
    validation: {
      minLength: number;
      maxLength: number;
      hasNumber: boolean;
    };
    touched: boolean;
    valid: boolean;
    error: string;
  };
}
