export interface IToast {
  class: string;
  message: string;
}
