import { IUser } from './IUser';
import { IUserData } from './IUserData';

export interface IAuthContext {
  user: IUser | null;
  userData: IUserData | null;
}
