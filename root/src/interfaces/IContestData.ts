export interface IContestData {
  title: string;
  category: string;
  coverImage: string;
  creator: string;
  createdOn: number;
  p1EndDate: number;
  p2EndDate: number;
  type: number;
  juryType: number;
  participants?: { [username: string]: boolean };
  jurors?: { [username: string]: boolean };
}
