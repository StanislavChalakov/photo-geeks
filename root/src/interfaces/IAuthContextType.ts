import { IAuthContext } from './IAuthContext';

export interface IAuthContextType extends IAuthContext {
  setAppState: React.Dispatch<React.SetStateAction<IAuthContext>>;
}
