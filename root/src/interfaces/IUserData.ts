export interface IUserData {
  uid: string;
  email: string | null;
  firstName: string;
  lastName: string;
  phoneNumber: string;
  username: string;
  registeredOn: number;
  address: string;
  avatar: string;
  role: number;
}
