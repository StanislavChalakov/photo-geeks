export interface IUser {
  email: string | null;
  uid: string;
}
