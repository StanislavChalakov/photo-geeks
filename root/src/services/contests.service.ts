import { ref, push, update, get } from 'firebase/database';
import { db } from '../firebase/config';
import { IContestData } from '../interfaces/IContestData';

export const createContest = async (
  title: string,
  category: string,
  coverImage: string,
  creator: string,
  p1Duration: number,
  p2Duration: number,
  type: number,
  juryType: number,
  participants: string[],
  jurors: string[]
) => {
  const p1DurationInMs = Math.floor(p1Duration * (1000 * 60 * 60 * 24));
  const p2DurationInMs = Math.floor(p2Duration * (1000 * 60 * 60));
  const contestData: IContestData = {
    title,
    category,
    coverImage,
    creator,
    type,
    juryType,
    createdOn: Date.now(),
    p1EndDate: Date.now() + p1DurationInMs,
    p2EndDate: Date.now() + p1DurationInMs + p2DurationInMs,
  };

  if (participants.length > 0) {
    contestData.participants = participants.reduce(
      (acc, participant) => ({ ...acc, [participant]: true }),
      {}
    );
  }

  if (jurors.length > 0) {
    contestData.jurors = jurors.reduce(
      (acc, jury) => ({ ...acc, [jury]: true }),
      {}
    );
  }

  const { key } = await push(ref(db, `contests`), contestData);

  return update(ref(db), {
    [`users/${creator}/createdContests/${key}`]: true,
  });
};

export const getAllContests = async () => {
  const snapshot = await get(ref(db, 'contests'));

  if (!snapshot.exists()) {
    return [];
  }

  return Object.keys(snapshot.val()).map((key) => ({
    ...snapshot.val()[key],
    id: key,
  }));
};

export const isTitleUnique = async (title: string) => {
  const allContests = await getAllContests();
  const isTitleFound = allContests.some((contest) => contest.title === title);

  if (isTitleFound) {
    throw new Error(`Contest with title '${title}' already exists!`);
  }
};
