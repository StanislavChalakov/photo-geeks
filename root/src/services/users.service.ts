import { ref, get, set, equalTo, orderByChild, query } from 'firebase/database';
import { db } from '../firebase/config';
import defaultAvatar from '../assets/defaultAvatar.jpg';
import { userRole } from '../common/user-role';

export const createUser = async (
  uid: string,
  email: string | null,
  firstName: string,
  lastName: string,
  username: string,
  phoneNumber: string,
  role: number = userRole.PHOTO_JUNKIE
) => {
  const userData = {
    uid,
    email,
    firstName,
    lastName,
    username,
    phoneNumber,
    address: '',
    role,
    avatar: defaultAvatar,
    registeredOn: Date.now(),
  };

  await set(ref(db, `users/${username}`), userData);

  return userData;
};

export const getUser = async (username: string) => {
  const snapshot = await get(ref(db, `users/${username}`));

  return snapshot.val();
};

export const getUserById = async (uid: string) => {
  const snapshot = await get(
    query(ref(db, 'users'), orderByChild('uid'), equalTo(uid))
  );

  const value = snapshot.val();

  if (value !== null) {
    return value[Object.keys(value)[0]];
  }

  return value;
};

export const getAllUsers = async () => {
  const snapshot = await get(ref(db, 'users'));

  if (!snapshot.exists()) {
    return [];
  }

  return Object.keys(snapshot.val()).map((key) => ({
    ...snapshot.val()[key],
    id: key,
  }));
};
