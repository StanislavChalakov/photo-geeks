import { useState, createContext, ReactNode } from 'react';
import { themeType } from '../common/theme-type.enum';

type ThemeContextObj = {
  theme: string;
  toggleTheme: () => void;
};

export const ThemeContext = createContext<ThemeContextObj>({
  theme: themeType.DARK,
  toggleTheme: () => {},
});

export const ThemeContextProvider: React.FC<{ children: ReactNode }> = ({
  children,
}) => {
  const [theme, setTheme] = useState(
    localStorage.getItem('theme') || themeType.DARK
  );

  const updateThemeHandler = () =>
    setTheme((prevTheme) => {
      const nextTheme =
        prevTheme === themeType.DARK ? themeType.LIGHT : themeType.DARK;
      localStorage.setItem('theme', nextTheme);

      return nextTheme;
    });

  const contextValue: ThemeContextObj = {
    theme: theme,
    toggleTheme: updateThemeHandler,
  };

  return (
    <ThemeContext.Provider value={contextValue}>
      {children}
    </ThemeContext.Provider>
  );
};
