import {
  ReactNode,
  createContext,
  useContext,
  useEffect,
  useState,
} from 'react';
import { useAuthState } from 'react-firebase-hooks/auth';
import { auth } from '../firebase/config';
import { getUserById } from '../services/users.service';
import { ToastContext } from './toast-context';
import { toastClass } from '../common/toast-class.enum';
import { IAuthContextType } from '../interfaces/IAuthContextType';
import { IAuthContext } from '../interfaces/IAuthContext';
import Spinner from '../UI/Spinner';

export const AuthContext = createContext<IAuthContextType>(
  {} as IAuthContextType
);

export const AuthContextProvider: React.FC<{
  children: ReactNode;
}> = ({ children }) => {
  const [user, loading] = useAuthState(auth);
  const [appState, setAppState] = useState<IAuthContext>({
    user: user ? { email: user.email, uid: user.uid } : null,
    userData: null,
  });
  const { addToast } = useContext(ToastContext);

  useEffect(() => {
    setAppState((appState) => ({
      ...appState,
      user: user ? { email: user.email, uid: user.uid } : null,
    }));
  }, [user]);

  useEffect(() => {
    if (appState.user !== null) {
      getUserById(appState.user.uid)
        .then((userData) =>
          setAppState((appState) => ({ ...appState, userData }))
        )
        .catch((e) => addToast(toastClass.ERROR, e.message));
    }
  }, [appState.user]);

  return (
    <AuthContext.Provider value={{ ...appState, setAppState }}>
      {loading ? <Spinner /> : children}
    </AuthContext.Provider>
  );
};
