import { ReactNode, createContext, useState } from 'react';
import { IToast } from '../interfaces/IToast';

type ToastContextObj = {
  toasts: IToast[];
  addToast: (toastClass: string, message: string) => void;
};

export const ToastContext = createContext<ToastContextObj>({
  toasts: [],
  addToast: () => {},
});

export const ToastContextProvider: React.FC<{ children: ReactNode }> = ({
  children,
}) => {
  const [toastsArr, setToastsArr] = useState<IToast[]>([]);

  const addToastHandler = (toastClass: string, message: string) => {
    const toast = {
      class: toastClass,
      message,
    };

    setToastsArr((toastsArr) => [...toastsArr, toast]);

    setTimeout(
      () => setToastsArr((toasts) => toasts.filter((t) => t !== toast)),
      4000
    );
  };

  const contextValue: ToastContextObj = {
    toasts: toastsArr,
    addToast: addToastHandler,
  };

  return (
    <ToastContext.Provider value={contextValue}>
      {children}
    </ToastContext.Provider>
  );
};
