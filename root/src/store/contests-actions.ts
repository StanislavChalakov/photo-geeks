import { getAllContests } from '../services/contests.service';
import { contestsActions } from './contests-slice';
import { AppDispatch } from './store';
import { uiActions } from './ui-slice';

export const fetchContestsData = () => {
  return async (dispatch: AppDispatch) => {
    dispatch(uiActions.toggleSpinner(true));

    try {
      const contestsData = await getAllContests();

      dispatch(contestsActions.setContests(contestsData));
    } catch (error) {
      dispatch(
        uiActions.toggleToast({
          showToast: true,
          toastErrorMessage: (error as Error).message,
        })
      );
    }

    dispatch(uiActions.toggleSpinner(false));
  };
};
