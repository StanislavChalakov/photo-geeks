import { createSlice } from '@reduxjs/toolkit';
import { IContestData } from '../interfaces/IContestData';

type initialStateType = {
  contests: IContestData[];
};

const initialState: initialStateType = {
  contests: [],
};

const contestsSlice = createSlice({
  name: 'contests',
  initialState,
  reducers: {
    setContests(state, action) {
      state.contests = action.payload;
    },
  },
});

export const contestsActions = contestsSlice.actions;

export default contestsSlice.reducer;
