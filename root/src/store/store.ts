import { configureStore } from '@reduxjs/toolkit';
import uiReducer from './ui-slice';
import constestsReducer from './contests-slice';

const store = configureStore({
  reducer: { ui: uiReducer, contests: constestsReducer },
});

export type RootState = ReturnType<typeof store.getState>;
export type AppDispatch = typeof store.dispatch;

export default store;
