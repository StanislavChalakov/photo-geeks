import { createSlice } from '@reduxjs/toolkit';

const uiSlice = createSlice({
  name: 'ui',
  initialState: {
    showSpinner: false,
    showToast: false,
    toastErrorMessage: '',
  },
  reducers: {
    toggleSpinner(state, action) {
      state.showSpinner = action.payload;
    },
    toggleToast(state, action) {
      state.showToast = action.payload.showToast;
      state.toastErrorMessage = action.payload.toastErrorMessage;
    },
  },
});

export const uiActions = uiSlice.actions;

export default uiSlice.reducer;
