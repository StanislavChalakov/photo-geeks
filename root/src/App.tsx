import { Route, Routes } from 'react-router-dom';
import './App.css';
import Home from './pages/Home';
import Leaderboard from './pages/Leaderboard';
import Contests from './pages/Contests';
import NewContest from './pages/NewContest';
import Login from './pages/Login';
import Register from './pages/Register';
import { useContext } from 'react';
import { AuthContext } from './providers/auth-context';
import { userRole } from './common/user-role';
import PageNotFound from './pages/PageNotFound';

function App() {
  const { userData } = useContext(AuthContext);

  return (
    <Routes>
      <Route path="/" element={<Home />} />
      <Route path="/leaderboard" element={<Leaderboard />} />
      <Route path="/contests" element={<Contests />} />
      {userData && userData.role === userRole.ORGANIZER && (
        <Route path="/new-contest" element={<NewContest />} />
      )}
      <Route path="/login" element={<Login />} />
      <Route path="/register" element={<Register />} />
      <Route path="/*" element={<PageNotFound />} />
    </Routes>
  );
}

export default App;
