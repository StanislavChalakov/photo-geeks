export const userInfoKeyLabel = {
  EMAIL: 'email',
  USERNAME: 'username',
  PASSWORD: 'password',
  FIRST_NAME: 'firstName',
  LAST_NAME: 'lastName',
  PHONE_NUMBER: 'phoneNumber',
  ADDRESS: 'address',
  AVATAR: 'avatar',
};
