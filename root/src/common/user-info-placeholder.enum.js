export const userInfoPlaceholder = {
  EMAIL: 'Email',
  USERNAME: 'Username',
  PASSWORD: 'Password',
  FIRST_NAME: 'First name',
  LAST_NAME: 'Last name',
  PHONE_NUMBER: 'Phone number',
  ADDRESS: 'Address',
  AVATAR: 'Avatar',
};
