import { PlusSmallIcon } from '@heroicons/react/24/outline';
import { NavLink } from 'react-router-dom';
import photoGeeksLogo from '../../assets/photoGeeksLogo.png';
import { useContext } from 'react';
import { AuthContext } from '../../providers/auth-context';
import { userRole } from '../../common/user-role';

export default function MainNav() {
  const { userData } = useContext(AuthContext);

  return (
    <nav className="btn-group items-start m-2">
      <NavLink
        to="/"
        className={({ isActive }) =>
          isActive ? 'btn btn-outline' : 'btn btn-ghost'
        }
        title="Home"
      >
        <img src={photoGeeksLogo} alt="main-logo" className="w-8 h-8" />
      </NavLink>
      <NavLink
        to="/leaderboard"
        className={({ isActive }) =>
          isActive ? 'btn btn-outline' : 'btn btn-ghost'
        }
        title="Leaderboard"
      >
        Leaderboard
      </NavLink>
      <NavLink
        to="/contests"
        className={({ isActive }) =>
          isActive ? 'btn btn-outline' : 'btn btn-ghost'
        }
        title="Contests"
      >
        Contests
      </NavLink>
      {userData && userData.role === userRole.ORGANIZER && (
        <NavLink
          to="/new-contest"
          className={({ isActive }) =>
            isActive ? 'btn btn-outline' : 'btn btn-ghost'
          }
          title="Create New Contest"
        >
          <PlusSmallIcon className="w-6 h-6" />
          New Contest
        </NavLink>
      )}
    </nav>
  );
}
