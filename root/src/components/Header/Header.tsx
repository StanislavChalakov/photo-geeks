import { Link } from 'react-router-dom';
import MainNav from './MainNav';
// import ProfileNav from './ProfileNav';
import Theme from './Theme';
import { useContext } from 'react';
import { AuthContext } from '../../providers/auth-context';
import ProfileNav from './ProfileNav';

export default function Header() {
  const { user } = useContext(AuthContext);

  return (
    <header className="sticky top-0 z-10">
      <div className="p-0 flex items-center justify-between">
        <MainNav />
        <div className="flex items-center">
          <Theme />
          {user !== null ? (
            <ProfileNav />
          ) : (
            <Link to={'/login'} className="btn btn-outline mx-2">
              Log in
            </Link>
          )}
        </div>
      </div>
    </header>
  );
}
