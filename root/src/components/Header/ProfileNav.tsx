import { useContext } from 'react';
import defaultAvatar from '../../assets/defaultAvatar.jpg';
import { logoutUser } from '../../services/auth.service';
import { AuthContext } from '../../providers/auth-context';
import { ToastContext } from '../../providers/toast-context';
import { toastClass } from '../../common/toast-class.enum';
import { useNavigate } from 'react-router-dom';

export default function ProfileNav() {
  const { setAppState } = useContext(AuthContext);
  const { addToast } = useContext(ToastContext);
  const navigate = useNavigate();

  const logoutHandler = async () => {
    await logoutUser();

    setAppState((appState) => ({
      ...appState,
      user: null,
      userData: null,
    }));

    addToast(toastClass.INFO, 'You have been logged out!');
    navigate('/');
  };

  return (
    <div className="flex items-center">
      <div className="flex">
        <img
          src={defaultAvatar}
          alt="avatar"
          className="rounded-lg h-12 w-12"
        />
        <div className="flex flex-col items-center justify-center mx-2">
          <span className="font-bold">Yoshicore</span>
          <span className="badge badge-secondary badge-sm badge-outline">
            Organizer
          </span>
        </div>
        <a onClick={logoutHandler} className="btn btn-outline mr-2">
          Logout
        </a>
      </div>
    </div>
  );
}
