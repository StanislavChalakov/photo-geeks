import { SunIcon, MoonIcon } from '@heroicons/react/24/outline';
import { useContext } from 'react';
import { themeType } from '../../common/theme-type.enum';
import { ThemeContext } from '../../providers/theme-context';

export default function Theme() {
  const { theme, toggleTheme } = useContext(ThemeContext);

  return (
    <label className="swap swap-rotate mr-2">
      <input
        type="checkbox"
        onChange={toggleTheme}
        defaultChecked={theme === themeType.DARK}
      />
      <SunIcon className="swap-off w-8 h-8" />
      <MoonIcon className="swap-on w-8 h-8" />
    </label>
  );
}
