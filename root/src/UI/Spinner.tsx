export default function Spinner() {
  return (
    <div className="fixed w-[100vw] flex justify-center top-[45vh]">
      <button className="btn btn-primary loading normal-case">Loading</button>
    </div>
  );
}
