import React, { useContext, useEffect } from 'react';
import { Helmet } from 'react-helmet';
import Footer from '../components/Footer/Footer';
import Header from '../components/Header/Header';
import { ThemeContext } from '../providers/theme-context';
import Toasts from '../components/Toasts/Toasts';
import { useAppDispatch, useAppSelector } from '../hooks/hooks';
import Spinner from './Spinner';
import { uiActions } from '../store/ui-slice';
import { ToastContext } from '../providers/toast-context';
import { toastClass } from '../common/toast-class.enum';

type Props = {
  children: React.ReactNode;
};

const Layout: React.FC<Props> = ({ children }: Props) => {
  const { addToast } = useContext(ToastContext);
  const { theme } = useContext(ThemeContext);
  const { showSpinner, showToast, toastErrorMessage } = useAppSelector(
    (state) => state.ui
  );
  const dispatch = useAppDispatch();

  useEffect(() => {
    if (showToast) {
      requestAnimationFrame(() => {
        addToast(toastClass.ERROR, toastErrorMessage);

        dispatch(
          uiActions.toggleToast({ showToast: false, toastErrorMessage: '' })
        );
      });
    }
  }, [showToast]);

  return (
    <>
      <Helmet htmlAttributes={{ 'data-theme': theme }} />
      <div className="App flex flex-col">
        <Header />
        <main className="grow">
          <div className="flex flex-col text-center text-xl items-center my-2 md:px-6 max-w-5xl mx-auto w-full">
            {children}
          </div>
          {showSpinner && <Spinner />}
        </main>
        <Toasts />
        <Footer />
      </div>
    </>
  );
};

export default Layout;
