/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ['./index.html', './src/**/*.{js,ts,jsx,tsx}'],
  theme: {
    extend: {},
  },
  daisyui: {
    themes: [
      {
        dark: {
          primary: '#4CA1AF',
          secondary: '#817df5',
          accent: '#c4fb10',
          neutral: '#ffffff',
          'base-100': '#0F2027',
          info: '#3ABFF8',
          success: '#34d399',
          warning: '#e68a00',
          error: '#f87171',
        },
        light: {
          primary: '#4CA1AF',
          secondary: '#817df5',
          accent: '#c4fb10',
          neutral: '#000000',
          'base-100': '#C4E0E5',
          info: '#3ABFF8',
          success: '#34d399',
          warning: '#e68a00',
          error: '#fc0303',
        },
      },
    ],
  },
  plugins: [require('daisyui')],
};
